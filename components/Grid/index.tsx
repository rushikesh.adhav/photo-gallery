import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Pressable,
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
} from 'react-native';
import {FetchedDataProps} from '../../types';
import {SingleItem} from '..';
import {API_URL, PAGE_SIZE} from '../../constants';
import {colors} from '../../utils/colors';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faBorderAll, faGripLines} from '@fortawesome/free-solid-svg-icons';
import {Dimensions} from 'react-native';
const Grid = () => {
  const [fetchedData, setFetchedData] = useState<FetchedDataProps[]>([]);
  const [columns, setColumns] = useState<number>(1);
  const [pageSize, setPageSize] = useState<number>(PAGE_SIZE);
  const [loading, setLoading] = useState<boolean>(true);

  const onOrientationChange = () => {
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;
    if (windowHeight < windowWidth) {
      setColumns(3);
    } else {
      setColumns(2);
    }
  };

  const renderFlatList = (numColumns: number) => (
    <FlatList
      data={fetchedData}
      renderItem={({item}) => (
        <SingleItem
          testId={'item'}
          title={item.title}
          url={item.url}
          columns={numColumns}
        />
      )}
      keyExtractor={item => item.id.toString()}
      numColumns={numColumns}
      onEndReached={() => setPageSize(pageSize + PAGE_SIZE)}
      // eslint-disable-next-line react/no-unstable-nested-components
      ListFooterComponent={() =>
        loading ? <ActivityIndicator size="large" color={colors.blue} /> : null
      }
      testID="flatList"
    />
  );

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch(`${API_URL}?_limit=${pageSize}`);
        const data = await res.json();
        setFetchedData(data);
        setLoading(false);
      } catch (error) {
        console.log(error, 'Error while fetching data');
      }
    };
    fetchData();
    const subscription = Dimensions.addEventListener(
      'change',
      onOrientationChange,
    );
    return () => subscription.remove();
  }, [pageSize]);
  return (
    <View>
      <View style={styles.buttonWrapper}>
        <View style={styles.buttonContainer}>
          <Pressable
            style={columns === 1 ? styles.btnWrapperLight : styles.btnWrapper}
            onPress={() => setColumns(1)}
            testID="pressable1">
            <FontAwesomeIcon icon={faGripLines} color={colors.blue} size={30} />
          </Pressable>
          <Pressable
            style={columns === 2 ? styles.btnWrapperLight : styles.btnWrapper}
            onPress={() => setColumns(2)}
            testID="pressable2">
            <FontAwesomeIcon icon={faBorderAll} color={colors.blue} size={30} />
          </Pressable>
        </View>
        <Text testID="cols" style={styles.gridText}>
          {columns} x {columns} Grid
        </Text>
      </View>
      <View style={styles.flatListContainer}>
        {columns === 1 ? renderFlatList(1) : null}
        {columns === 2 ? renderFlatList(2) : null}
        {columns === 3 ? renderFlatList(3) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  buttonWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
    position: 'absolute',
    right: 20,
  },
  gridText: {
    marginRight: 20,
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    gap: 20,
    marginHorizontal: 20,
    marginVertical: 10,
  },
  btnWrapper: {
    padding: 8,
    borderRadius: 5,
  },
  btnWrapperLight: {
    backgroundColor: colors.lightBlue,
    padding: 8,
    borderRadius: 5,
  },
  flatListContainer: {marginTop: 100},
});

export default Grid;
