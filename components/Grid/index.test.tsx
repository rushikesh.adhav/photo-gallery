import React from 'react';
import {render, waitFor, fireEvent} from '@testing-library/react-native';
import Grid from '.';
import {PAGE_SIZE} from '../../constants';

jest.mock('@fortawesome/react-native-fontawesome', () => ({
  FontAwesomeIcon: '',
}));

jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve([]),
  }),
);

test('displays the correct number of items based on PAGE_SIZE', async () => {
  const {queryAllByTestId} = render(<Grid />);
  await waitFor(() => {
    const items = queryAllByTestId('item');
    expect(items.length).toBe(PAGE_SIZE);
  });
});

test('displays single item in a row', async () => {
  const {queryAllByTestId} = render(<Grid />);
  await waitFor(() => {
    const pressable1 = queryAllByTestId('pressable1')[0];
    fireEvent.press(pressable1);
    const columns = queryAllByTestId('cols')[0];
    expect(columns.props.children[0]).toBe(1);
  });
});

test('displays two items in a row', async () => {
  const {queryAllByTestId} = render(<Grid />);
  await waitFor(() => {
    const pressable2 = queryAllByTestId('pressable2')[0];
    fireEvent.press(pressable2);
    const columns = queryAllByTestId('cols')[0];
    expect(columns.props.children[0]).toBe(2);
  });
});

it('loads more items when the user scrolls to the bottom', async () => {
  const {queryAllByTestId} = render(<Grid />);
  await waitFor(async () => {
    const flatList = queryAllByTestId('flatList')[0];
    const initialItems = queryAllByTestId('item').length;
    fireEvent.scroll(flatList, {
      nativeEvent: {
        contentOffset: {y: 1000},
        contentSize: {height: 2000},
        layoutMeasurement: {height: 500},
      },
    });
    await waitFor(() => {
      const updatedItems = queryAllByTestId('item').length;
      expect(updatedItems).toBeGreaterThan(initialItems);
    });
  });
});
