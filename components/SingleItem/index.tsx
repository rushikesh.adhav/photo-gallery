import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {SingleItemProps} from '../../types';
import {colors} from '../../utils/colors';

const SingleItem = ({title, url, columns, testId}: SingleItemProps) => {
  if (columns === 3) {
    return (
      <View testID={testId} style={styles.singleItemContainerLandscape}>
        <Image source={{uri: url}} style={styles.mainImage} />
        <Text style={styles.titleText}>{title.slice(0, 10)}</Text>
      </View>
    );
  } else {
    return (
      <View
        testID={testId}
        style={
          columns === 1
            ? styles.singleItemContainerRow
            : styles.singleItemContainer
        }>
        <Image source={{uri: url}} style={styles.mainImage} />
        <Text style={styles.titleText}>{title.slice(0, 10)}</Text>
      </View>
    );
  }
};
const styles = StyleSheet.create({
  singleItemContainer: {
    width: '50%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 20,
  },
  singleItemContainerLandscape: {
    width: '33%',
    display: 'flex',
    marginBottom: 20,
    marginHorizontal: 25,
  },
  singleItemContainerRow: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 28,
    marginBottom: 20,
  },
  titleText: {fontSize: 17, color: colors.black},
  mainImage: {width: 150, height: 150, borderRadius: 10},
});

export default SingleItem;
