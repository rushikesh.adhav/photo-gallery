import React from 'react';
import {SafeAreaView} from 'react-native';
import {Grid} from './components';

function App(): JSX.Element {
  return (
    <SafeAreaView>
      <Grid />
    </SafeAreaView>
  );
}

export default App;
