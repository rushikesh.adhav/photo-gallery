export const colors = {
  white: '#fff',
  black: '#000',
  blue: '#39A7FF',
  lightBlue: '#87C4FF',
  accent: '#FFEED9',
};
