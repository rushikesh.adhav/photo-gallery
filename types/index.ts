export interface FetchedDataProps {
  id: number;
  title: string;
  url: string;
}
export interface SingleItemProps {
  title: string;
  url: string;
  columns: number;
  testId: string;
}
